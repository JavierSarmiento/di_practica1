<?php
require 'ConexionSingleton.php';
class Usuario{

    private $NOMBRE;
    private $CLAVE;
    private $F_INICIO;
    private $F_FIN;
    private $AUTH_KEY;

    function __construct($nombre=null, $clave=null, $f_inicio=null, $f_fin=null, $auth_key=null)
    {
        $this->NOMBRE = $nombre;
        $this->CLAVE = $clave;
        $this->F_INICIO = $f_inicio;
        $this->F_FIN = $f_fin;
        $this->AUTH_KEY = $auth_key;
    }
    public static function checkAUTH_KEY($auth_key) {
        try {
            $db = BD::getInstance();
            BD::setCharsetEncoding();
            $sqlSelect = 'SELECT * FROM registros WHERE auth_key = :auth_key';
            $stm = $db->prepare($sqlSelect);
            $stm->bindParam(':auth_key', $auth_key);
            $stm->execute();
            $resultado = $stm->fetchAll(PDO::FETCH_ASSOC);
            foreach($resultado as $a){
               $pauth_key=$a["auth_key"];
                if($pauth_key==$auth_key) {
                      return TRUE;
                }else{
                      return FALSE;
                }
             }
  
        } catch (Exception $e) {
        throw new Exception($e->getMessage(), 1);
        }
    }
    public static function checkLogin($nombre, $clave) {
        try {
            $clavehash=hash('sha256', $clave);
            $db = BD::getInstance();
            BD::setCharsetEncoding();
            $compClave = 'SELECT nombre,clave FROM registros WHERE nombre = :nombre';
            $stm = $db->prepare($compClave);
            $stm->bindParam(':nombre', $nombre);
            $stm->execute();
            $resultado = $stm->fetchAll(PDO::FETCH_ASSOC);
            foreach($resultado as $a){
               $pclave=$a["clave"];
                if($pclave==$clavehash) {  return TRUE;  }else{   return FALSE;
                }
             }
 
        } catch (Exception $e) {
        throw new Exception($e->getMessage(), 1);
        }
    }
    public static function setAUTH_KEY($nombre) {
        try {
            $db = BD::getInstance();
            BD::setCharsetEncoding();
            $sqlAuth = 'UPDATE registros SET auth_key = :auth_key WHERE nombre = :nombre';
            $stm = $db->prepare($sqlAuth);
            $stm->bindParam(':nombre', $nombre);
            $auth_key=hash('sha256', $nombre);
            $stm->bindParam(':auth_key', $auth_key);
            $stm->execute();
            return $auth_key;
        } catch (Exception $e) {
        throw new Exception($e->getMessage(), 1);
        }
    }
}