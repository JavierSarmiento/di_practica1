<?php
require "../clases/Usuario.php";
    
$tipo = isset($_POST['tipo'])? $_POST['tipo']:'json';
$nombre = isset($_POST["nombre"]) ? $_POST["nombre"]:'null' ;
$clave = isset($_POST["clave"]) ? $_POST["clave"]:'null'  ;
$auth_KEY = isset($_POST["auth_KEY"]) ? $_POST["auth_KEY"]:'null';

if (empty($tipo)){
    $tipo = 'json';
}
    
try {
    switch ($tipo) {
    case 'json':
            $json = json_encode(array(
            "success"=>true,
            "msg"=>"Datos del login.",
            "auth_key"=> Usuario::setAUTH_KEY($nombre),
            "data"=>Usuario::checkLogin($nombre,$clave) 
        ));
    break;
    default:
        $json = json_encode(array(
        "success"=>false,
        "msg"=>"Formato no soportado."
    ));
    break;
    }       

} catch (Exception $e) {
    $json = json_encode(array(
    "success"=>false,
    "msg"=>$e->getMessage()
));
}

echo $json;
exit();

