<?php

require 'Usuario.php';
// Par�metros de entrada
$auth_key = isset($_POST['auth_key']) ? $_POST["auth_key"]:null;

// JSON
$json_error = json_encode(array(
        "success"=>false,
        "msg"=>"No tienes permisos para acceder a esta secci�n.",
        "auth_key"=>"$auth_key",
));
$logued = Usuario::checkAUTH_KEY($auth_key);
// Si no est� definido el auth_key retorna JSON de error
if ( !$logued ) {
echo $json_error;
// Finaliza para que no ejecute el c�digo del archivo que lo incluya
exit();
}